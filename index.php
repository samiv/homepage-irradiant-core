<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" initial-scale="1.0" />
  <meta name="description" content="Irradiant Core is an action shooter mobile game, combined with real time strategy and role playing game features, and takes place in a world where natural light is a scarce resource. Main character is a humanoid-like cyborg. When progressing in game, main character can acquire more powerful equipment and unlock and upgrade different skills. Read more!" />
  <meta name="theme-color" content="#035766" />
  <meta name="msapplication-navbutton-color" content="#035766">
  <meta name="apple-mobile-web-app-status-bar-style" content="#035766">
  <title>Irradiant Core - Action Shooter Mobile Game</title>
  <link type="text/css" rel="stylesheet" href="css/master.css?v=5" />
  <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Bangers' />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  <link rel="icon" href="images/GameLogo/Core_Icon_512.png" />
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="js/cookiesFunctions.js?v=4"></script>
</head>

<?php
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
  //ip from share internet
  $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
  //ip pass from proxy
  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
  $ip = $_SERVER['REMOTE_ADDR'];
}
$ip = $_SERVER['REMOTE_ADDR'];
$data = file_get_contents('data/visitordata_irradiantcore.json');
$json_arr = json_decode($data, true);
$ipfound = false;

if (is_array($json_arr) || is_object($json_arr)) {
  foreach ($json_arr as $key => $value) {
    if ($value['ip'] == $ip) {
      $json_arr[$key]['counter'] = $json_arr[$key]['counter'] + 1;
      $ipfound = true;
      break;
    }
  }
}
if (!$ipfound) {
  $json_arr[] = array('ip' => $ip, 'counter' => 1);
}
$testi = json_encode($json_arr);
file_put_contents('data/visitordata_irradiantcore.json', json_encode($json_arr));
?>

<script>
  function sendVisit() {
    $.ajax({
      url: "https://sekvensoft-backend.herokuapp.com/add",
      type: "POST",
      contentType: 'application/json',
      data: JSON.stringify({
        ip: `<?php echo $ip ?>`,
        site: 'IRRADIANT_CORE'
      })
    });
  }
</script>

<body onload="sendVisit()">

  <div class="wrapper">

    <!-- HEADER -->
    <header>
      <div id="logo-irradiant-core">
        <a href="http://irradiantcore.com/">
          <img src="images/GameLogo/IrradiantLogo_plus_icon.png" alt="LOGO" />
        </a>
      </div>

      <div class="main_info">
        <div>Out Now!</div>
      </div>

      <div class="navigation_logo">
        <a class="" href="http://sekvensoft.com" target="_blank">
          <img src="images/logo_trans_with_text_fitted.png" alt="" /></a>
      </div>
    </header>


    <!-- END HEADER -->

    <div id="slideshow">
      <div class="slideshow_images">
        <a class="leftArrow" onclick="plusSlideshowSlides(-1)">&#10094;</a>
        <img class="slides animation-slide" src="images/Banner/IrradiantCore_banner_1067x600.jpg" alt="Front page" />
        <img class="slides animation-slide" src="images/release_screenshots_13022021/IC_Screenshot_1.jpg" alt="Game screenshot" />
        <img class="slides animation-slide" src="images/release_screenshots_13022021/IC_Screenshot_2.jpg" alt="Game screenshot" />
        <img class="slides animation-slide" src="images/release_screenshots_13022021/IC_Screenshot_4.jpg" alt="Game screenshot" />
        <img class="slides animation-slide" src="images/release_screenshots_13022021/IC_Screenshot_5.jpg" alt="Game screenshot" />
        <img class="slides animation-slide" src="images/release_screenshots_13022021/IC_Screenshot_7.jpg" alt="Game screenshot" />
        <img class="slides animation-slide" src="images/release_screenshots_13022021/IC_Screenshot_9.jpg" alt="Game screenshot" />
        <img class="slides animation-slide" src="images/release_screenshots_13022021/IC_Screenshot_12.jpg" alt="Game screenshot" />
        <img class="slides animation-slide" src="images/release_screenshots_13022021/IC_Screenshot_13.jpg" alt="Game screenshot" />
        <ul class="pagination_list">
          <li class="pagination_item">
            <a class="circle pagination_active" onclick="carousel(1)"></a>
          </li>
          <li class="pagination_item">
            <a class="circle" onclick="carousel(2)"></a>
          </li>
          <li class="pagination_item">
            <a class="circle" onclick="carousel(3)"></a>
          </li>
          <li class="pagination_item">
            <a class="circle" onclick="carousel(4)"></a>
          </li>
          <li class="pagination_item">
            <a class="circle" onclick="carousel(5)"></a>
          </li>
          <li class="pagination_item">
            <a class="circle" onclick="carousel(6)"></a>
          </li>
          <li class="pagination_item">
            <a class="circle" onclick="carousel(7)"></a>
          </li>
          <li class="pagination_item">
            <a class="circle" onclick="carousel(8)"></a>
          </li>
          <li class="pagination_item">
            <a class="circle" onclick="carousel(9)"></a>
          </li>
        </ul>
        <a class="rightArrow" onclick="plusSlideshowSlides(1)">&#10095;</a>
      </div>
      <!-- slideshow closing tag -->
    </div>

    <div class="testers_wrapper">
      <div class="testers_text">
        Hi There! We're currently looking for new closed Alpha testers.
      </div>
      <a href="#links_container" onclick="setTestersCookies()"><button type="button">View More</button></a>
      <button onclick="closeTesters()">Close</button>
    </div>

    <div id="links_container">
      <a href="https://twitter.com/IrradiantCore" target="_blank">
        <i class="fab fa-twitter"></i>
      </a>
      <a href="https://www.facebook.com/IrradiantCore/" target="_blank">
        <i class="fab fa-facebook"></i>
      </a>
      <a href="https://www.instagram.com/sekvensoft/" target="_blank">
        <i class="fab fa-instagram"></i>
      </a>
      <a href="https://discord.gg/M62Y5kp" target="_blank">
        <i class="fab fa-discord"></i>
      </a>
      <a href="https://www.youtube.com/channel/UCIus2s0x6nkq7bFjw_zAL_A" target="_blank">
        <i class="fab fa-youtube"></i>
      </a>
    </div>

    <div id="content">
      <div class="testers-content">
        <div class="testers-content-text">
          We're currently looking for new closed <br /> Alpha testers with an Android device.
          <br /><br />You may sign up in our Discord server:
          <a target="_blank" href="https://discord.gg/QufBywN">discord.gg/QufBywN</a>
          <br /><br />Or fill the form: <a href="https://goo.gl/forms/WPuM1QeolmSvPq7Y2">goo.gl/forms/WPuM1QeolmSvPq7Y2</a>
          <br /><br />Or contact us via email:
          <a href="mailto:contact@sekvensoft.com">contact@sekvensoft.com</a>
        </div>
      </div>
      <div id="basic-info">
        <div class="logo-container">
          <img src="images/GameLogo/IrradiantLogo_plus_icon.png" alt="Irradiant Core Logo">
          <span class="emphasis">Irradiant Core </span> is a new challenging sci-fi single player action shooter
          combined with real time strategy and role-playing game features. It takes place in a world where natural
          light is a scarce resource. You control a humanoid cyborg who can acquire more powerful gear and learn new skills.
          Irradiant Core offers exciting, fun and challenging content with a rewarding, in-depth, character progression.
        </div>

        <div class="trailer">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/Jipt_9sSPSs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div id="twitter_left">
          <a class="twitter-timeline" data-lang="en" data-width="500" data-height="700" href="https://twitter.com/IrradiantCore?ref_src=twsrc%5Etfw">Tweets by IrradiantCore</a>
          <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>

        <div id="credits">
          <div id="app_images_container">
            <div class="credits_header">Out now on <br /> Android</div>
            <a href="https://play.google.com/store/apps/details?id=com.Sekvensoft.IrradiantCore" target=_blank>
              <img class="app_logo" src="images/googleplay.png" alt="Google Play Store">
            </a>
            <!--<img class="app_logo" src="images/appstore.png" alt="App Store"> -->
          </div>
          <div id="developed_container">
            <div class="credits_header">Developed by</div>
            <a href="http://sekvensoft.com">
              <img src="images/logo_black_with_text512.png" alt="Sekvensoft">
            </a>
          </div>
        </div>

        <div class="contact_info">Any questions on your mind? Don't hesitate to <a href="http://sekvensoft.com/#contact" target="_blank">Contact Us</a> </div>
      </div>

      <div id="twitter_right">
        <!-- <div id="current_status_right">
        Current Status: <br/> Closed Alpha Stage
      </div> -->
        <a class="twitter-timeline" data-lang="en" data-width="500" data-height="1150" href="https://twitter.com/IrradiantCore?ref_src=twsrc%5Etfw">Tweets by IrradiantCore</a>
        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
      </div>

      <!-- CLOSING CONTENT -->
    </div>
    <!-- CLOSING WRAPPER -->
  </div>

  <!-- COOKIES-MODAL -->
  <?php include 'cookiesPopup.html'; ?>

  <!-- FOOTER -->
  <?php include 'defaultFooter.html'; ?>

  <script>
    $(document).ready(function() {
      $('.toggle').click(function() {
        if ($(".navigation_bar").css('display') == 'none') {
          $(".navigation_bar").css('display', 'block');
          $(".navigation_bar").height(0);
          $(".toggle").css('color', '#0e90b0');
          $(".toggle").hover(
            function() {
              $(this).css('color', '#0e90b0');
            });
        } else if ($(".navigation_bar").css('display') == 'block') {
          $(".navigation_bar").css('display', 'none');
          $(".toggle").css('color', 'white');
          $(".toggle").hover(

            function() {
              $(this).css('color', '#0e90b0');
            },

            function() {
              $(this).css('color', 'white');
            }

          );
        }
      })
    })

    $(window).resize(function() {
      if ($(this).width() >= 992) {
        $('.navigation_bar').css('display', 'flex');
        $('.navigation_bar').css('height', '100%');
      } else {
        $('.navigation_bar').css('display', 'none');
      }
    });

    function hideNavbar() {
      if ($(this).width() < 992) {
        $('.navigation_bar').css('display', 'none');
      }
    }

    // Check if cookie isAcceptedTerms is set and show popup if needed
    checkIsAcceptedCookie();
    checkTestersCookie();

    var slideIndex = 0;
    var slideTimer = null;
    carousel(1);

    function carousel(n) {
      if (slideTimer !== null) {
        clearTimeout(slideTimer);
        slideTimer = null;
      }
      var i;
      var x = document.getElementsByClassName("slides");
      for (i = 0; i < x.length; ++i) {
        x[i].style.display = "none";
      }
      slideIndex = n;
      if (slideIndex > x.length) {
        slideIndex = 1;
      }
      if (slideIndex < 1) {
        slideIndex = x.length;
      }
      x[slideIndex - 1].style.display = "block";

      var pagination_items = document.getElementsByClassName("circle");
      for (i = 0; i < pagination_items.length; ++i) {
        pagination_items[i].className = pagination_items[i].className.replace(" pagination_active", "");
      }
      pagination_items[slideIndex - 1].className += " pagination_active";
      // If timeout time is changed, it should also be changed in master.css -> .animation-slide
      slideTimer = setTimeout(carousel, 8000, slideIndex + 1);
    }

    function plusSlideshowSlides(n) {
      carousel(slideIndex += n);
    }

    var modal = document.getElementById('cookies-modal');
    window.onclick = function(event) {
      if (event.target == modal) {
        closeCookiesModal();
      }
    }
  </script>

  </div>

</body>

</html>