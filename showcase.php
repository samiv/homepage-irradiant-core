<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <meta name="theme-color" content="#035766" />
  <meta name="msapplication-navbutton-color" content="#035766">
  <meta name="apple-mobile-web-app-status-bar-style" content="#035766">
  <title>Irradiant Core</title>
  <link type="text/css" rel="stylesheet" href="css/master.css?v=1" />
  <link type="text/css" rel="stylesheet" href="css/showcase.css?v=1" />
  <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Bangers' />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  <link rel="icon" href="images/GameLogo/Core_Icon_512.png" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/cookiesFunctions.js"></script>
</head>
<body>
  <div class="wrapper">

    <!-- HEADER -->
    <header id="myHeader">
      <?php include 'defaultHeader.html';?>
      <div class="navigation_bar">
        <ul class="nav-list">
          <li class="nav-item">
            <a href="http://irradiantcore.com/">HOME</a>
          </li>
          <li class="nav-item">
            <a class ="hdractive" href="#C1">SHOWCASE</a>
          </li>
          <li class="nav-item">
            <a  href="http://irradiantcore.com/qa">Q&A</a>
          </li>
        </ul>
      </div>
    </header>

    <div id="showcase_content">
      <h1 class="page_header_txt">Showcase material during development</h1>

      <div id="showcase_videos">
        <div class="milestone">Beta</div>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/HdbH7j3AqeM" frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <div class="milestone">Alpha</div>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/Hqf0oVqHAUs"
        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/61O4fOpSvM0"
        frameborder="0" allow="autoplay; encrypted-media;" allowfullscreen></iframe>
        <iframe src="https://www.youtube.com/embed/51W8y_B-zkc"
        frameborder="0" allow="autoplay; encrypted-media;" allowfullscreen></iframe>
      </div>

      <div id="showcase_images">
        <div class="milestone">Beta</div>
        <img src="images/beta_scteenshots_01092019/beta_screenshot1_330.jpg" alt="" onclick="openModal(1)">
        <img src="images/beta_scteenshots_01092019/beta_screenshot2_330.jpg" alt="" onclick="openModal(2)">
        <img src="images/beta_scteenshots_01092019/beta_screenshot3_330.jpg" alt="" onclick="openModal(3)">
        <img src="images/beta_scteenshots_01092019/beta_screenshot4_330.jpg" alt="" onclick="openModal(4)">
        <img src="images/beta_scteenshots_01092019/beta_screenshot5_330.jpg" alt="" onclick="openModal(5)">
        <img src="images/beta_scteenshots_01092019/beta_screenshot6_330.jpg" alt="" onclick="openModal(6)">

        <div class="milestone">Alpha</div>
        <img src="images/screenshots_18012019/priority_etusivu_587.jpg" alt="" onclick="openModal(7)">
        <img src="images/screenshots_18012019/screenshot1_587.jpg" alt="" onclick="openModal(8)">
        <img src="images/screenshots_18012019/screenshot3_587.jpg" alt="" onclick="openModal(9)">
        <img src="images/screenshots_18012019/screenshot4_587.jpg" alt="" onclick="openModal(10)">
        <img src="images/screenshots_18012019/screenshot5_587.jpg" alt="" onclick="openModal(11)">
        <img src="images/screenshots_18012019/eietusivu_587.jpg" alt="" onclick="openModal(12)">
      </div>
    </div>

    <!-- Close wrapper -->
  </div>

  <!-- COOKIES-MODAL -->
  <?php include 'cookiesPopup.html';?>

  <!-- FOOTER -->
  <?php include 'defaultFooter.html';?>

  <div id="myModal" class="modal">
    <span class="close_modal" onClick="closeModal()">&times;</span>
    <div class="modal_content">
      <div class="mySlide">
        <img class="modal_content" src="images/beta_scteenshots_01092019/beta_screenshot1_1080.jpg" alt="">
        <div class="caption">Beta snapshot</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/beta_scteenshots_01092019/beta_screenshot2_1080.jpg" alt="">
        <div class="caption">Beta snapshot</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/beta_scteenshots_01092019/beta_screenshot3_1080.jpg" alt="">
        <div class="caption">Beta snapshot</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/beta_scteenshots_01092019/beta_screenshot4_1080.jpg" alt="">
        <div class="caption">Beta snapshot</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/beta_scteenshots_01092019/beta_screenshot5_1080.jpg" alt="">
        <div class="caption">Beta snapshot</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/beta_scteenshots_01092019/beta_screenshot6_1080.jpg" alt="">
        <div class="caption">Beta snapshot</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/screenshots_18012019/priority_etusivu.jpg" alt="">
        <div class="caption">Kaboom!</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/screenshots_18012019/screenshot1.jpg" alt="">
        <div class="caption">Just destroying stuff</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/screenshots_18012019/screenshot3.jpg" alt="">
        <div class="caption">Check that new piece of equipment</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/screenshots_18012019/screenshot4.jpg" alt="">
        <div class="caption">Allied tanks doing their job</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/screenshots_18012019/screenshot5.jpg" alt="">
        <div class="caption">That nasty Mammoth Tank</div>
      </div>
      <div class="mySlide">
        <img class="modal_content" src="images/screenshots_18012019/eietusivu.jpg" alt="">
        <div class="caption">Enemy's base is conquered!</div>
      </div>
      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
      <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
  </div>

  <script type="text/javascript">

  $(document).ready(function(){
    $('.toggle').click(function(){
      if ($(".navigation_bar").css('display') == 'none') {
        $(".navigation_bar").css('display', 'block');
        $(".navigation_bar").height(0);
        $(".toggle").css('color', '#0e90b0');
        $(".toggle").hover(
          function() {
            $(this).css('color', '#0e90b0');
          });
        }
        else if ($(".navigation_bar").css('display') == 'block') {
          $(".navigation_bar").css('display', 'none');
          $(".toggle").css('color', 'white');
          $(".toggle").hover(

            function() {
              $(this).css('color', '#0e90b0');
            },

            function() {
              $(this).css('color', 'white');
            }

          );
        }
      })
    })

    $(window).resize(function(){
      if($(this).width() >= 992){
        $('.navigation_bar').css('display', 'flex');
        $('.navigation_bar').css('height', '100%');
      }
      else {
        $('.navigation_bar').css('display', 'none');
      }
    });

    checkIsAcceptedCookie();

    var slideIndex = 1;

    function openModal(idx) {
      slideIndex = idx;
      $("#myModal").show();
      showSlide(idx);
    }

    function showSlide(n) {
      var slides = $(".mySlide");
      if (n > slides.length) {slideIndex = 1}
      if (n < 1) {slideIndex = slides.length}
      for(var i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
      }
      slides[slideIndex-1].style.display = "block";
    }

    function plusSlides(n) {
      showSlide(slideIndex += n);
    }

    function closeModal() {
      $("#myModal").hide();
    }

    var modal = document.getElementById('myModal');

    window.onclick = function(event) {
      if (event.target == modal) {
        closeModal();
        // modal.style.display = "none";
      }
    }

    $(document).keydown(function(e) {
      if (e.keyCode == 27) {
        // Escape
        closeModal();
      }
      else if (e.keyCode == 37) {
        // Left arrow
        if($("#myModal").is(':visible')) {
          plusSlides(-1);
        }
      }
      else if (e.keyCode == 39) {
        // Right arrow
        if($("#myModal").is(':visible')) {
          plusSlides(1);
        }
      }
    });

    var modal = document.getElementById('cookies-modal');
    window.onclick = function(event) {
      if (event.target == modal) {
        closeCookiesModal();
      }
    }
    </script>
  </body>
  </html>
