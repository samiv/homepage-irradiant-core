<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" initial-scale="1.0" />
    <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Bangers' />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="../css/visitorData/login_visitordata.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
</head>

<script type="text/javascript">
  $(document).ready(function() {
    $("form").submit(function(event) {
      event.preventDefault();
      var username = $("#login-user").val();
      var password = $("#login-password").val();
      $.ajax({
        url: "login_validate.php",
        data: {
          "username": username,
          "password": password,
          "submit": true
        },
        dataType: 'json',
        type: 'POST',
        beforeSend: function() {
          $("#login-user, #login-password").removeClass("input-error");
        },
        success: function(data) {

          if(data.retValue == 1 && data.error == "") {
            window.location.replace('./get_visitordata_irradiantcore.php');
          }
          else if(data.retValue == 0) {
            if(data.error == "user") {
              $(".login-error").text("Username field is empty");
              $("#login-user").addClass("input-error");
            }
            else if(data.error == "pw") {
              $(".login-error").text("Password field is empty");
              $("#login-password").addClass("input-error");
            }
            else if(data.error == "wrong") {
              $(".login-error").text("Wrong password");
              $("#login-user, #login-password").addClass("input-error");
            }
            else if(data.error == "submit") {
              $(".login-error").text("Submitting login info failed");
            }
            else {
              $(".login-error").text("Error occured");
            }
          }
        }
      });
    });
  });
</script>

<div class="wrapper">
  <div class="logo-container">
    <img src="../images/GameLogo/IrradiantLogo_plus_icon.png" alt="IrradiantCore Logo">
  </div>
  <div class="form-content">
    <form action="login_validate.php" method="post">
      <h3>Login to visitor-data page</h3>
      <input id="login-user" type="text" name="username" placeholder="Username">
      <input id="login-password" type="password" name="password" placeholder="Password">
      <input type="submit" value="Submit">
      <p class="login-error"></p>
    </form>
  </div>
</div>


</html>
