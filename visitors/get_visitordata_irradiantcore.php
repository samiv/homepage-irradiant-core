<!DOCTYPE html>

<?php
ob_start();
session_start();
if (isset($_SESSION['user_id'])) {
  $userid = $_SESSION['user_id'];
  echo "<div id=user-info><a id=logout-btn href='./logout.php'>Logout</a>";
  echo "<span id='user-info-txt'>You are logged in as <span class='emp'>$userid</span></span>";
  echo "</div>";

}
else {
  header('Location: /visitors/login.php');
}
?>

<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" initial-scale="1.0" />
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <link type="text/css" rel="stylesheet" href="../css/visitorData/get_visitordata.css" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

  <script>
  $(document).ready(function() {

    $("input").keyup(function() {
      var searchString = $("input").val();
      $.post("filterIps.php", {
        suggestion: searchString
      }, function(data, status) {
        $("#test").html(data);
      });
    });

    $("#showHide").click(function() {
      $('.ips').toggle();
      if ($(this).text() == "Show all visits") {
        $(this).text("Hide all visits");
      } else {
        $(this).text("Show all visits");
      }
    });

  });
  </script>
</head>

<body>

  <div class="content">

    <div class="logo">
      <img src="../images/GameLogo/IrradiantLogo_plus_icon.png" alt="IrradiantCore Logo">
    </div>

    <div class="data">

      <h1>VISITORS AT IRRADIANTCORE.COM</h1>

      <?php

      $data = file_get_contents('../data/visitordata_irradiantcore.json');
      $json_arr = json_decode($data, true);

      $visits = 0;
      $visitors = 0;
      foreach ($json_arr as $key => $value) {
        ++$visitors;
        $visits = $visits + $json_arr[$key]['counter'];
      }
      echo "<h2> Number of visits at irradiantcore.com: <span class='number'>$visits</span> </h2>";
      echo "<h2> Number of unique visitors (by ip) at irradiantcore.com: <span class='number'>$visitors</span></h2>";
      ?>

      <button id='showHide' type='button' name='button'>Show all visits</button>

      <div class="ips">
        All visitors and visitcounts (x?) by IP <br/><br/>
        Filter IPs:
        <input type="text" name="filter" value="" />
        <p id="test">
          <?php
          foreach ($json_arr as $key => $value) {
            echo $json_arr[$key]['ip'];
            echo " (x ";
            echo $json_arr[$key]['counter'];
            echo ") <br/>";
          }
          ?>
        </p>
      </div> <!-- ips -->

    </div> <!-- data -->

  </div> <!-- content -->

</body>

</html>
