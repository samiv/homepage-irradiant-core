<?php

$data = file_get_contents('../data/visitordata_irradiantcore.json');
$existingIps = json_decode($data, true);

if(isset($_POST['suggestion'])) {
  $ip = $_POST['suggestion'];

  if(!empty($ip)) {
    foreach ($existingIps as $key => $value) {
      if(strpos($existingIps[$key]['ip'], $ip) !== false) {
        echoIpLine($existingIps, $key);
      }
    }
  }
  else {
    foreach ($existingIps as $key => $value) {
      echoIpLine($existingIps, $key);
    }
  }
}

function echoIpLine($array, $k) {
  echo $array[$k]['ip'];
  echo " (x ";
  echo $array[$k]['counter'];
  echo ") <br/>";
}

?>
