﻿<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <meta name="theme-color" content="#035766" />
  <meta name="msapplication-navbutton-color" content="#035766">
  <meta name="apple-mobile-web-app-status-bar-style" content="#035766">
  <title>Irradiant Core</title>
  <link type="text/css" rel="stylesheet" href="css/master.css?v=1" />
  <link type="text/css" rel="stylesheet" href="css/qa.css?v=1" />
  <link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Bangers' />
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  <link rel="icon" href="images/GameLogo/Core_Icon_512.png" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/cookiesFunctions.js"></script>
</head>
<body>
  <div class="wrapper">

    <!-- HEADER -->
    <header id="myHeader">
      <?php include 'defaultHeader.html';?>
      <div class="navigation_bar">
        <ul class="nav-list">
          <li class="nav-item">
            <a href="http://irradiantcore.com/">HOME</a>
          </li>
          <li class="nav-item">
            <a href="http://irradiantcore.com/showcase">SHOWCASE</a>
          </li>
          <li class="nav-item">
            <a class ="hdractive" href="#C1">Q&A</a>
          </li>
        </ul>
      </div>
    </header>
    <!-- END HEADER -->

    <div id="faq_content">
      <h1 class="page_header_txt">Q&A</h1>

      <button class="faq_question">What is Irradiant Core?</button>
      <div class="faq_answer">
          Irradiant Core is a mid-core action shooter mobile game combined with real time strategy and role-playing game features and takes place in a world where natural light is a scarce resource. Main character is a humanoid-like cyborg who can acquire more powerful equipment and learn new skills. Goal is to offer exciting, fun, vibrant and challenging content with rewarding, in-depth, character progression.
      </div>
      <button class="faq_question">Is the game a single player or multiplayer game?</button>
      <div class="faq_answer">
		  The first release of the game will be a single player with different game modes to play, namely Campaign and Survival Challenge.
      </div>
      <button class="faq_question">For which platforms the game will be released?</button>
      <div class="faq_answer">
		  First version of the game will be targeted for mobile devices such as smart phones and tablets and will be released on Android in 2020. After the release on mobile, we are also planning to port the game on consoles such as Nintendo Switch.
      </div>
      <button class="faq_question">What is the revenue model of the game?</button>
      <div class="faq_answer">
          Mobile release of the game is designed to be a free-to-play, which seemed to be the only option for an indie game. All in-app products are completely optional, hence only for convenience and to remove ads. On top of that, products can be also acquired without using real money. This is because we believe in “no-paywalls, never pay-to-win” -philosophy.
      </div>
      <button class="faq_question">Have you raised any funds for the development?</button>
      <div class="faq_answer">
        No. Irradiant Core is crafted with passion without any funding or external support. We have used our own money though.
      </div>
      <button class="faq_question">Have you tell more about the goal in the game?</button>
      <div class="faq_answer">
		<p>In Campaign mode, main character must find his/her way through different battlegrounds by conquering enemy’s base using tactics and allied combat machines, such as self-guided tanks and turrets, while defending his own base from enemy’s machinery. Battlegrounds will also incorporate controllable machinery such as turrets. Each instanced-like battleground will have different difficulties to choose from. The higher is the difficulty, the greater is the chance to obtain better rewards, such as new gear and in-game currency.</p>
		<p>In Survival Challenge, main character must destroy waves of enemies to advance to new battlegrounds, until defeated or time limit is reached. Each round will be more challenging. Player will gain score based on how far player has advanced.</p>
      </div>
      <button class="faq_question">What do you mean by “in-depth character progression”?</button>
      <div class="faq_answer">
        <p>By playing the game content, player will gain experience and level up. Each level will give player one attribute and skill point as well as improve player's trait effects. Attribute and skill points can be used to upgrade player’s attributes and to unlock and upgrade skills, respectively.</p>
		<p>Progress from common to legendary gear. Equip new gear sets to unlock different set bonuses. Enhance a piece of equipped gear to improve its properties.</p>
		<p>Update and improve your allied machinery.</p>
		<p>The game will encourage to test different builds to find the one which fits best to player's game style.</p>
      </div>
	  <button class="faq_question">How are the controls implemented?</button>
      <div class="faq_answer">
        The game offers two options for controls. Player can either use a joystick with automatic targeting or "tap-to-move" and "tap-to-target". These two control options can be also used jointly, for example, joystick for movement with tap-to-target. In terms of controls, the main challenge is to actively dodge incoming projectiles and avoid other threats in real time.
      </div>
    </div>

    <!-- Close wrapper -->
  </div>

  <!-- COOKIES-MODAL -->
  <?php include 'cookiesPopup.html';?>

  <!-- FOOTER -->
  <?php include 'defaultFooter.html';?>

  <script>

  $(document).ready(function(){
    $('.toggle').click(function(){
      if ($(".navigation_bar").css('display') == 'none') {
        $(".navigation_bar").css('display', 'block');
        $(".navigation_bar").height(0);
        $(".toggle").css('color', '#0e90b0');
        $(".toggle").hover(
          function() {
            $(this).css('color', '#0e90b0');
          });
      }
      else if ($(".navigation_bar").css('display') == 'block') {
        $(".navigation_bar").css('display', 'none');
        $(".toggle").css('color', 'white');
        $(".toggle").hover(

          function() {
            $(this).css('color', '#0e90b0');
          },

          function() {
            $(this).css('color', 'white');
          }

        );
      }
    })
  })

  $(window).resize(function(){
    if($(this).width() >= 992){
      $('.navigation_bar').css('display', 'flex');
      $('.navigation_bar').css('height', '100%');
    }
    else {
      $('.navigation_bar').css('display', 'none');
    }
  });

  checkIsAcceptedCookie();

  var acc = document.getElementsByClassName("faq_question");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var faq_answer = this.nextElementSibling;
      if (faq_answer.style.maxHeight){
        faq_answer.style.maxHeight = null;
        faq_answer.style.padding = "0px";
      } else {
        faq_answer.style.maxHeight = faq_answer.scrollHeight + 13 + "px";
        faq_answer.style.padding = "13px";
      }
    });
  }

  var modal = document.getElementById('cookies-modal');
  window.onclick = function(event) {
    if (event.target == modal) {
      closeCookiesModal();
    }
  }
  </script>
</body>
</html>
