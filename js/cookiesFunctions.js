function checkIsAcceptedCookie() {
  var isAccepted = getIsAcceptedCookie("acceptedIrr");
  if (isAccepted != "") {
    hideCookiesPopup();
  }
}

function getIsAcceptedCookie(isAccepted) {
  var accepted = isAccepted + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(accepted) == 0) {
      return c.substring(accepted.length, c.length);
    }
  }
  return "";
}

function setIsAcceptedCookie(isAccepted, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = isAccepted + "=" + cvalue;
  document.cookie = expires;
  document.cookie = ";path=/";
}

function hideCookiesPopup() {
  $(".cookies-popup").fadeOut();
  setIsAcceptedCookie("acceptedIrr", "true", 7);
}

function showCookiesModal() {
  hideCookiesPopup();
  $("#cookies-modal").fadeIn();
}

function closeCookiesModal() {
  $("#cookies-modal").fadeOut();
}

function checkTestersCookie() {
  var testers = getIsAcceptedCookie("testers");
  if (testers != "") {
    closeTesters();
  }
}

function setTestersCookies() {
  document.cookie = "testers=true";
  closeTesters();
  $('.testers-content-text').css('backgroundColor','hsl(40, 86%, 51%)');

var d = 1000;
for(var i=51; i>=0; i=i-0.45){ //i represents the lightness
    d  += 10;
    (function(ii,dd){
        setTimeout(function(){
            $('.testers-content-text').css('backgroundColor','hsl(40,86%,'+ii+'%)');
        }, dd);
    })(i,d);
}

}

function closeTesters() {
  $(".testers_wrapper").fadeOut("slow");
}
